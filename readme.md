### Spring AOP 事务管理

#### JDBC事务管理的痛点
  事务管理是应用开发中不能回避的一个重要问题。那么我们如何在JDBC中使用事务
处理呢？也就是让 JDBC 把多条 SQL 语句当成一个事务来处理。通过 DriverManager 对
象获取到 Connection 对象之后，Connection 对象处于自动提交模式下，
这意味着它在执行每个语句后都会自动提交。
```
//刚获取到的connection处于自动提交模式
Connection conn=DriverManager.getConnection(...);
```
如果需要用JDBC进行事务管理，需要做的工作是：
```
Connection conn=DriverManager.getConnection(...);
conn.setAutoCommit(false);
... 业务代码

conn.commit();//提交事务

```
如果禁用自动提交模式，为了提交更改，必须显式调用commit 方法；否则无法保存数据库更改。

在 jdbc 中事务控制是靠 Connection 来进行的，并且要求同一个事务中所有的操作
都要使用同一个连接。而且事务控制应该在业务层中进行。   

为了进行事务控制，业务层中必须要使用 Connection，而 DAO
也要使用 Connection，也就是要将 Connection 对象在各个层次之间传递，这样破坏了
三层结构的基本原则.利用 Spring 的 AOP，可以将事务管理的代码 “分离”出来。

### 使用Spring进行事务控制-准备工作
- 引入spring-context
- 引入spring-aop
- 引入spring-tx 事务控制
- 引入spring-jdbc 
- 引入oracle jdbc驱动包
- 引入commons-dbcp 连接池
- 在oracle中创建表
- aspectjweaver
```
create table TX_TEST
(
  title VARCHAR2(30),
  name  VARCHAR2(30)
)
```

- 定义dao接口并实现
```
public interface TransactionTestDAO {
	public void save(String title,String name);
}

```
### 使用Spring进行事务控制-在配置文件中配置
- 配置数据源
- 自动扫描
- 配置事务管理器，这个事务管理器
```
	<!-- 配置事务管理器，需要注入datasource -->
	<bean id="txManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
 		<property name="dataSource" ref="dataSource"/>
	</bean>
```
- 配置事务管理的advice
```
	<!-- 事务管理advice -->
	<tx:advice id="txAdvice" transaction-manager="txManager">
		<tx:attributes>
			<tx:method name="save*" propagation="REQUIRED" isolation="DEFAULT" />
			<tx:method name="query*" read-only="true" />
		</tx:attributes>
	</tx:advice>
```

- 配置切面
```
	<!-- 配置切面 -->
	<aop:config>
		<!-- 
		expression 表示选择 join point 的表达式，这个表达式 包含几个部分：
		
		1. * 表示方法的返回值为任意类型
		2.  cn.itlaobing.dao 包以及子包 (两个点表示子包)
		3. *表示任何类
		4.  . 类中的方法
		5. save* 表示已save开头的方法
		6. () 就是参数
		7.  .. 表示参数可以是任何参数
		 -->
		<aop:pointcut expression="execution(* cn.itlaobing.dao..*.save*(..))" id="txPointcut"/>
		<aop:advisor advice-ref="txAdvice" pointcut-ref="txPointcut"/>
	</aop:config>
```





### 使用Spring进行事务控制-使用annotation



