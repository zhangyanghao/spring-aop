package cn.itlaobing.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import cn.itlaobing.dao.BaseDao;
import cn.itlaobing.dao.TransactionTestDAO;

@Repository
public class TransactionTestDaoImpl extends BaseDao implements TransactionTestDAO {
	
	Log log=LogFactory.getLog(TransactionTestDaoImpl.class);

	@Override
	public void save(String title, String name) {
		
		String sqlA="insert into tx_test values(?,?)";
		
		super.getJdbcTemplate().update(sqlA,"title A","name A");
		
		boolean flag=true;
		if(flag){
			throw new RuntimeException("手工引发一个异常");
		}
		
		String sqlB="insert into tx_test values(?,?)";
		super.getJdbcTemplate().update(sqlB,"title B","name B");
	}
	
	
}
