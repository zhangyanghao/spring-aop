package cn.itlaobing.springaop.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cn.itlaobing.dao.TransactionTestDAO;

public class TranactionDaoTest extends BaseTest{
	
	
	@Autowired
	private TransactionTestDAO transactionTestDAO;
	
	@Test
	public void testSave(){
		System.out.println(transactionTestDAO.getClass().getName());
		transactionTestDAO.save("", "");
	}

}
